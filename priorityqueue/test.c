#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "pq.h"

int main(void)
{
    char *test[] = {"one", "two", "three", "four"};
    int i = 0;
    pqueue pq;

    pqueue_init(&pq);
    
    for(i = 0; i < 4*100; i++)
        pqueue_insert(&pq, i, test[i % 4]);

    for(i = 0; i < 4*100; i++)
        assert(!strcmp((char*)pqueue_front(&pq), test[i % 4]));


    assert(pq.elements == 0);

    for (i = 0; i < 4; i++)
        pqueue_insert(&pq, 4 - i, test[i]);

    for (i = 0; i < 4; i++)
        assert(!strcmp((char*)pqueue_front(&pq), test[3 - i]));

    assert(pq.elements == 0);
    pqueue_deinit(&pq);
    return 0;
}

    
