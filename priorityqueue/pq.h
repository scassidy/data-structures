#ifndef PQ_H_
#define PQ_H_

#ifndef PQ_INIT_SIZE
#define PQ_INIT_SIZE 32
#endif

typedef struct
{
	int prio;
	void *data;
} pq_keydata;

typedef struct
{
	pq_keydata *q;
	size_t size;
	size_t elements;
} pqueue;

/**
 * pqueue_init
 *
 * Creates a priority queue
 */
void pqueue_init(pqueue *pq);

/**
 * pqueue_deinit
 *
 * Frees all data associated with priority queue
 */
void pqueue_deinit(pqueue *pq);

/**
 * pqueue_insert
 *
 * Inserts data into the priority queue at the specified priority.
 * Lower number is higher priority (min heap)
 *
 * Returns list if successful, NULL otherwise
 */
pqueue* pqueue_insert(pqueue* list, int priority, void *data);

/**
 * pqueue_front
 *
 * Pops the highest priority item off of the queue and returns it.
 *
 * Returns NULL if there were no items in the list
 */
void* pqueue_front(pqueue* list);


/**
 * pqueue_peek
 *
 * If any items are left in the queue, returns the priority
 * of the first element
 */
int pqueue_peek(pqueue* list);

#endif /* PQ_H_ */
