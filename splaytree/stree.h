#ifndef __STREE_H
#define __STREE_H

#ifndef STREE_CMP_INT
typedef int (*stree_cmp_func)(void *, void *);
typedef void* skey_t;
#else
typedef int stree_cmp_func;
typedef int skey_t;
#endif

struct _stree;

typedef struct _stree {
    skey_t key;
    void *data;
    struct _stree *left;
    struct _stree *right;
    struct _stree *parent;
} streen;

typedef struct {
    stree_cmp_func f;
    streen *root;
    int num;
} stree;

#ifdef STREE_CMP_INT
#define stree_insert_int(s, key, data) \
    stree_insert(s, key, data)
#define stree_init_int(s, key, data) \
    stree_init(s, 0, key, data)
#define stree_find_int(s, key) \
    stree_find(s, key)
#endif

void  stree_init(stree *s, stree_cmp_func f, skey_t key, void *data);
void  stree_insert(stree *s, skey_t key, void *data);
void *stree_find(stree *s, skey_t key);
void  stree_free(stree *s);

#endif
