$(ODIR)/%.o: %.c $(HDRS)
	$(CC) -c -o $@ $< $(CFLAGS)

all: splaytree radixtrie priorityqueue

.PHONY: clean 

splaytree: force
	$(MAKE) -C splaytree

radixtrie: force
	$(MAKE) -C radixtrie

priorityqueue: force
	$(MAKE) -C priorityqueue

clean:
	$(MAKE) -C splaytree clean
	$(MAKE) -C radixtrie clean
	$(MAKE) -C priorityqueue clean

force:
	true
